package net7.xyz.removesms.util;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import net7.xyz.removesms.R;
import net7.xyz.removesms.model.DataSMS;

import java.util.List;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<DataSMS> mValues;
    //private final OnListFragmentInteractionListener mListener;

    public MyItemRecyclerViewAdapter(List<DataSMS> items) {
        mValues = items;
        //mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.dateSms.setText(mValues.get(position).getDate());
        holder.addressSms.setText(mValues.get(position).getAddress());
        holder.bodySms.setText(mValues.get(position).getBody());
        holder.typeSms.setText(mValues.get(position).getFolderName());


       /* holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    //mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date_sms)
        TextView dateSms;

        @BindView(R.id.address_sms)
        TextView addressSms;

        @BindView(R.id.body_sms)
        TextView bodySms;

        @BindView(R.id.type_sms)
        TextView typeSms;


        public DataSMS mItem;

        public ViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);

        }

    }
}
