package net7.xyz.removesms.model;

/**
 * Created by user on 12.02.2018.
 */

public class DataSMS {
    String address;
    String body;
    String date;//received
    String person;//phonebook
    String subject;
    private String folderName;

    public DataSMS() {
        this.address = "";
        this.body = "";
        this.date = "";
        this.person = "";
        this.subject = "";
    }


    public String getAddress() {
        return address;
    }

    public String getBody() {
        return body;
    }

    public String getDate() {
        return date;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {

        this.folderName = folderName;
    }

}
