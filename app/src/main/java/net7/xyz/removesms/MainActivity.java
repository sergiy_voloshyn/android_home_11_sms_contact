package net7.xyz.removesms;

import android.net.Uri;
import android.provider.Telephony;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import net7.xyz.removesms.model.DataSMS;
import net7.xyz.removesms.util.MyItemRecyclerViewAdapter;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.list)
    RecyclerView listViewRecycler;

    @BindView(R.id.text_to_find)
    TextView textToFind;
    MyItemRecyclerViewAdapter adapter;

    int PERMISSION_ALL = 1;

    String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_SMS, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS};

    List<DataSMS> inboxSMSList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (hasPermission(PERMISSIONS)) {
            // readContacts();
            readSMS();
        } else {
            askPermission();
        }
    }

    private void askPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
    }

    private boolean hasPermission(String... permissions) {

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_ALL &&
                grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            readSMS();
        }
    }

    public void readSMS() {

        inboxSMSList = getAllSms("");

        if (inboxSMSList.size() != 0) {
            listViewRecycler.setLayoutManager(new LinearLayoutManager(this));
            adapter = new MyItemRecyclerViewAdapter(inboxSMSList);
            listViewRecycler.setAdapter(adapter);


        } else {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("No SMS found!")
                    .setMessage("SMS not found!")
                    .setCancelable(true)
                    .show();
        }

    }


    public List<DataSMS> getAllSms(String strToFind) {

        List<DataSMS> lstSms = new ArrayList<>();
        ContentResolver cr = getContentResolver();
        Uri message = Uri.parse("content://sms/");

        Cursor c = cr.query(message,
                new String[]{Telephony.Sms.ADDRESS, Telephony.Sms.BODY, Telephony.Sms.DATE, Telephony.Sms.PERSON, Telephony.Sms.SUBJECT,
                        Telephony.Sms.TYPE}, Telephony.Sms.BODY + " LIKE ?", new String[]{"%" + strToFind + "%"},
                Telephony.Sms.DEFAULT_SORT_ORDER);

        int totalSMS = c.getCount();

        if (c.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {
                DataSMS dataSMS = new DataSMS();
                dataSMS.setAddress(c.getString(0) != null ? c.getString(0) : "");
                dataSMS.setBody(c.getString(1) != null ? c.getString(1) : "");

                Date date = new Date(c.getLong(2));
                String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
                dataSMS.setDate(formattedDate);

                dataSMS.setPerson(c.getString(3) != null ? c.getString(3) : "");


                dataSMS.setSubject(c.getString(4) != null ? c.getString(4) : "");

                String messageType = c.getString(c.getColumnIndexOrThrow("type"));
                if (messageType.contains("1")) {
                    dataSMS.setFolderName("inbox");
                } else if (messageType.contains("2")) {
                    dataSMS.setFolderName("sent");
                } else if (messageType.contains("3")) {
                    dataSMS.setFolderName("draft");
                } else if (messageType.contains("4")) {
                    dataSMS.setFolderName("outbox");
                } else if (messageType.contains("5")) {
                    dataSMS.setFolderName("failed");
                } else {
                    dataSMS.setFolderName("unknown");
                }
                lstSms.add(dataSMS);
                c.moveToNext();
            }
        } else {
            Log.d("Read sms", "getAllSmsFromProvider: ");
            return lstSms;
        }
        c.close();

        return lstSms;
    }


    public int deleteSmsByString(String strToFind) {

        ContentResolver cr = getContentResolver();
        Uri message = Uri.parse("content://sms/");
        int result = 0;
        int delCount = 0;
        try {
            Cursor c = cr.query(Telephony.Sms.CONTENT_URI,
                    new String[]{Telephony.Sms.ADDRESS, Telephony.Sms.BODY, Telephony.Sms.DATE, Telephony.Sms.PERSON, Telephony.Sms.SUBJECT,
                            Telephony.Sms.TYPE, Telephony.Sms._ID}, Telephony.Sms.BODY + " LIKE ?", new String[]{"%" + strToFind + "%"},
                    null);

            int totalSMS = c.getCount();

            if (c.moveToFirst()) {
                for (int i = 0; i < totalSMS; i++) {
                    int id = c.getInt(c.getColumnIndexOrThrow(Telephony.Sms._ID));
                    //long id = c.getInt(6);
                    delCount = this.getContentResolver().delete(Uri.parse("content://sms/" + id), null, null);
                    c.moveToNext();
                    result = result + delCount;
                }
            }
        } catch (Exception e) {
            Log.d("Delete sms", "delete SMS: ");
        }
        return result;
    }

    @OnClick(R.id.find_button)
    void onClickFindButton() {

        inboxSMSList.clear();
        inboxSMSList.addAll(getAllSms(textToFind.getText().toString()));

        if (inboxSMSList.size() != 0) {
            adapter.notifyDataSetChanged();

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("SMS found!")
                    .setMessage("SMS found, \n" +
                            " total count: " + Integer.toString(inboxSMSList.size()) + " SMS.")
                    .setCancelable(true)
                    .show();

        } else {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("No SMS found!")
                    .setMessage("SMS not found!")
                    .setCancelable(true)
                    .show();
        }
    }

    @OnClick(R.id.delete_button)
    void onClickDeleteButton() {

        String strToDelete = textToFind.getText().toString();
        inboxSMSList.clear();
        inboxSMSList.addAll(getAllSms(strToDelete));

        if (inboxSMSList.size() != 0) {
            adapter.notifyDataSetChanged();
            int countDeleted = deleteSmsByString(strToDelete);
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Deleted SMS !")
                    .setMessage("Count : " + Integer.toString(countDeleted) + " SMS deleted!")
                    .setCancelable(true)
                    .show();

        } else {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("No SMS found!")
                    .setMessage("SMS not found!")
                    .setCancelable(true)
                    .show();
        }
    }

    public static final String SPACE = " ";

    private void readContacts() {
        // get ContentResolver
        ContentResolver cr = getContentResolver();
        //create query
        Cursor cur = cr.query(Contacts.CONTENT_URI,
                new String[]{Contacts._ID, Contacts.DISPLAY_NAME, Contacts.HAS_PHONE_NUMBER},
                null,
                null,
                Contacts._ID + SPACE + "ASC");

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(0);
                String name = cur.getString(1);
                Log.i("userName", id + " " + name);

                if (cur.getInt(2) > 0) {
                    getPhoneNumbers(id);
                }
            }
        }
        //don’t forget
        cur.close();
    }

    private List<String> getPhoneNumbers(String id) {
        List<String> list = new ArrayList<>();
        Cursor pCur = getContentResolver().query(
                Phone.CONTENT_URI, // table name
                new String[]{Phone.NUMBER}, // select only numbers
                Phone.CONTACT_ID + " = ?", // where contact id == user id
                new String[]{id}, // user id
                null); // without sorting
        while (pCur.moveToNext()) {
            String phoneNo = pCur.getString(0);
            list.add(phoneNo);
            Log.i("userName", phoneNo);
        }
        pCur.close();
        return list;
    }


}